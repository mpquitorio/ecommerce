<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('main');

Route::prefix('pages')->group(function () {
    Route::get('/about', 'AboutController@index')->name('about');

    Route::get('/carts', 'CartController@index')->name('carts.all');
    Route::post('/carts/add/{id}', 'CartController@add')->name('carts.add');
    Route::get('/carts/empty', 'CartController@empty')->name('carts.empty');
    Route::post('/carts/add', 'CartController@update');
    Route::get('/carts/remove/{id}', 'CartController@remove')->name('carts.remove');
    Route::post('/carts/checkout/{type}', 'CartController@checkout');

    Route::get('/products', 'ProductController@index')->name('products');
    Route::get('/products/show/{id}', 'ProductController@show')->name('products.show');
    Route::get('/products/search', 'ProductController@search')->name('products.search');
    Route::get('/products/category/{id}', 'ProductController@selectedCategory')->name('selectedCategory');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


