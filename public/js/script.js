$(document).ready(function(){

    function wowInit() {
        new WOW().init();
    }
    wowInit();

    ///prevent both backspace and delete keys in input field (qty)
    $(".quantity").keypress(function (e) {
        e.preventDefault();
    }).keydown(function(e){
        if ( e.keyCode === 8 || e.keyCode === 46 ) {
            return false;
        }
    });

     // password hide & show
     $('.pass_show').append('<span class="ptxt">Show</span>'); 

     $('.ptxt').on('click', function(){ 
         $(this).text($(this).text() == "Show" ? "Hide" : "Show"); 
         $(this).prev().attr('type', function(index, attr){return attr == 'password' ? 'text' : 'password'; }); 
     });  

    let qtys = $('.qty');

    for (qty of qtys){
        $(qty).on("change", updateQty);
    }

    function updateQty(){

        let newQty = $(this).val();
        //console.log(`The product quantity is: ${newQty}`);

        let price  = $(this).attr("data-price");
        //console.log(`The product price is: ${price}`);

        let product_id = $(this).attr("data-product-id");
        //console.log(`The product ID is: ${product_id}`);

        let updatedValue = newQty * price; 

        let parent = $(this).parent();

        $(parent).next().children().next().html( number_format(updatedValue.toFixed(2)) );
        
        subTotal();

        /*----------  Update the SESSION["cart"] using Fetch API ----------*/

        // call the csrf-token
        let token = $('meta[name="csrf-token"]').attr('content');      

        //  Create Form Data to hold the values to pass to /pages/carts/add
        let formData = new FormData();
        formData.append("qty", newQty);
        formData.append("product_id", product_id);

        fetch('/pages/carts/add', {
            headers:{
                "X-CSRF-TOKEN": token
            },
        	method: 'POST',
        	body: formData
        })
        .then(function (response){
            return response.text();
        }).then(function(text){
            console.log(text);
        }).catch(function (error){
            console.log(error)
        })

        /*---------- End of Update  SESSION["cart"] using Fetch API ----------*/ 

    }

    function subTotal(){

        let productsSubTotal = $('.sub');

        //console.log(productsSubTotal);

        let sum = 0;
        for(let i = 0; i < productsSubTotal.length; i++){
            sum += parseFloat( $(productsSubTotal[i]).html().replace(",","") );
        }
        $('#subtotal').html( '₱ ' + number_format(sum.toFixed(2)) );
        $('#total').html( '₱ ' + number_format(sum.toFixed(2)) );
    }

    function number_format(num) {
        let n = num.toString().split(".");
        n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return n.join(".");
    }

});
