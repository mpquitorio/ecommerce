<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert(
            [
                [ 
                    'name' => 'Product 1',
                    'price' => '600.00',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 2',
                    'price' => '850.00',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 3',
                    'price' => '456.30',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '2',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 4',
                    'price' => '897.90',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '2',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 5',
                    'price' => '542.80',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '3',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 6',
                    'price' => '227.90',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '3',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 7',
                    'price' => '899.35',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '3',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 8',
                    'price' => '120.35',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '3',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 9',
                    'price' => '1000.35',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '3',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Product 10',
                    'price' => '2987.00',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English.',
                    'img_path' => 'https://via.placeholder.com/250x250',
                    'category_id' => '2',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
            ]
        );
    }
}
