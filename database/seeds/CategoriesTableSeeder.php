<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [ 
                    'name' => 'Category 1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Category 2',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Category 3',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ], 
                [ 
                    'name' => 'Category 4',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ]
            ]
        );
    }
}
