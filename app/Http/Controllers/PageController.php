<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class PageController extends Controller
{
    public function index()
    {
        $items = Item::inRandomOrder()->limit(4)->get();
        return view('index')->with('items', $items);
    }
}
