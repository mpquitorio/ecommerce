<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Order;
use App\OrderItem;
use Session;

class CartController extends Controller
{
    public function index()
    {
        $cart = session()->get('cart');
        $item_ids = (!empty($cart)) ? array_keys($cart) : [];
        $items = Item::find($item_ids);

        return view('pages.carts.index', ['items' => $items]);
    }

    public function add(Request $request, $id)
    {
        // get product id and quantity
        $quantity = $request->input('qty');
        
        // check the session cart if exist
        if($request->session()->exists('cart.'.$id)) 
        {
            $currentQuantity = $request->session()->get('cart.'.$id);
		    $request->session()->put('cart.'.$id, $quantity + $currentQuantity);
        } 
        else 
        {
            $request->session()->put('cart.'.$id, $quantity);
        }

        Session::flash('success', 'Item(s) added to cart');
        return redirect()->route('products');
    }

    public function update(Request $request)
    {
        // get product id and quantity 
        $id = $request->input('product_id');
        $qty = $request->input('qty');

        // Remove the product 
        $request->session()->forget('cart.'. $id);

        // Add the product with the updated quantity
        $request->session()->put('cart.'.$id, $qty);
    }

    public function remove(Request $request, $id)
    {
        $request->session()->forget('cart.'. $id);
        return redirect()->route('carts.all');
    }

    public function empty(Request $request)
    {
        session()->forget('cart');
        return redirect()->route('carts.all');
    }

    public function checkout(Request $request, $type)
    {
        if (\Auth::check()) 
        {
            $cart = session()->get('cart');
  
            $item_ids = array_keys($cart);
    
            $items = Item::find($item_ids);
    
            $order = new Order;
            $order->user_id = auth()->user()->id;
            $order->price = $request->input('total');
            $order->type = $type;
            $order->save();
      
            foreach($items as $item)
            {
                $orderItems = new OrderItem;
                $orderItems->order_id = $order->id;
                $orderItems->item_id = $item->id;
                $orderItems->qty = $cart[$item->id];
                $orderItems->save();
            }

            $request->session()->forget('cart');

            Session::flash('success', 'Thank you for shopping!');
            return redirect()->route('home');
        } 
        else 
        {
            return redirect()->route('login');
        }
        
       
    }

}
