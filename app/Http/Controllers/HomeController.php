<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Order;
use App\OrderItem;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userID = auth()->user()->id;
        $orders = Order::where('user_id', $userID)->orderBy('created_at', 'desc')->get();

        $datas = DB::table('users')
                ->join('orders', 'orders.user_id', '=', 'users.id')
                ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                ->join('items', 'items.id', '=', 'order_items.item_id')
                ->select('items.name', 'items.price', 'order_items.qty', 'orders.id')
                ->get();

        return view('home', compact('datas'))->with('orders', $orders);
    }
}
