@extends('layouts.app')

@section('content')

    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5 mt-5">
                    <fieldset class="mt-4">     
                        <legend>USER LOGIN</legend>
                        <div class="text-center my-3">
                            <a href="#"><img src="{{ asset('images/fb_login.png') }}" alt="FB Login" class="fb_login"></a><br>
                            <small>We will never post on your behalf or share any information <br>without your permission.</small>
                        </div>
                        <div class="hr_login text-center my-4">
                            <span>or</span>
                        </div>
                        <form action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-user"></i></div>
                                </div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="{{ __('E-Mail Address') }}" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="input-group mb-2 pass_show">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-lock"></i></div>
                                </div>
                                <input id="password" type="password" class="form-control pr-5 @error('password') is-invalid @enderror" name="password" placeholder="{{ __('Password') }}" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror 
                            </div>   
                            <div class="form-group d-flex d-inline">
                                <div>
                                    @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}" id="forgotpassword">
                                        <small>{{ __('Forgot Your Password?') }}</small>
                                    </a>
                                @endif
                                </div>
                                <div class="custom-control custom-checkbox" style="margin-left: 175px">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <small class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </small>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-block btn-primary" id="userLogin">
                                {{ __('Login') }}
                            </button>
                        </form>
                        <div class="text-muted text-center mt-3">
                            Don't have an account?<a href="{{ route('register') }}" class="ml-2" id="register">Sign Up</a>
                        </div>
                        <div class="text-muted text-center my-2">
                            <small>By signing in, you agree to our <a href="#" id="TermService">Terms of Service</a> & <a href="#" id="PrivacyPolicy">Privacy Policy</a></small>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </main>

@endsection
