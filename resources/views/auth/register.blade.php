@extends('layouts.app')

@section('content')

    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-5 mt-5">
                    <fieldset class="my-4">     
                        <legend>CREATE AN ACCOUNT</legend>
                        <div class="text-center my-3">
                        <a href="#"><img src="{{ asset('images/fb_login.png') }}" alt="FB Login" class="fb_login"></a><br>
                            <small>We will never post on your behalf or share any information <br>without your permission.</small>
                        </div>
                        <div class="hr_login text-center my-4">
                            <span>or</span>
                        </div>
                        <form action="{{ route('register') }}" method="POST">
                            @csrf
                            <div>
                                <label class="font-weight-bold">{{ __('E-Mail Address') }} *</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <small class="text-muted">We'll send messages to the above email address. Please ensure the email address is accessible and up-to-date.</small>
                            </div>
                            <div class="mt-3">
                                <label class="font-weight-bold">{{ __('Password') }} *</label>
                                <div class="input-group pass_show">
                                    <input id="password" type="password" class="form-control pr-5 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="mt-3">
                                <label class="font-weight-bold">{{ __('Confirm Password') }} *</label>
                                <div class="input-group pass_show">
                                    <input id="password-confirm" type="password" class="form-control pr-5" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            <div class="mt-4">
                                <label class="font-weight-bold">{{ __('Name') }} * <small>- what should we call you? </small></label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="text-muted text-center my-2">
                                <small>By creating your account, you agree to our <a href="#" id="TermService">Terms of Service</a> & <br><a href="#" id="PrivacyPolicy">Privacy Policy</a></small>
                            </div>
                            <button type="submit" class="btn btn-block btn-primary">
                                {{ __('Register') }}
                            </button>
                        </form>
                        <div class="text-muted text-center my-2">
                            <small><a id="signin-link" href="{{ route('login') }}">I already have an account!</a></small>
                        </div>
                    </fieldset> 
                </div>
            </div>
        </div>
    </main>

@endsection
