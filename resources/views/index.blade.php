@extends('layouts.app')

@section('content')

    <main>
        <!-- Jumbotron -->
        <div class="jumbotron wow fadeIn">
            <div class="container">
                <h1 class="display-3">The DEMO Shop</h1>
                <p>The DemoShop is an exclusive fashion community where select boutiques showcase unique products & styles online.</p>
                <p>As country's Online Fashion Destination, we create endless style possibilities through an ever-expanding range of products form the most coveted international and local brands, putting you at the centre of it all. <strong class="font-weight-bold">With DemoShop, You Own Now.<strong></p>
                <p><a class="btn btn-outline-primary" href="{{ route('products') }}"> Shop Now <i class="fas fa-angle-right"></i></a></p>
            </div>
        </div>

        <!-- Featured Items -->
        <div class="container">
            <h2 class="wow slideInRight">Featured Items</h2>
            <hr>
            <div class="row text-center wow fadeIn">
            <!-- Retrieve records from products table and display here using class "card" in bootstrap-->
                @foreach ($items as $item)
                    <div class="col col-sm-12 col-md-4 col-lg-3 mb-3 d-inline-block">
                        <div class="card h-100">
                            <img src="{{ $item->img_path }}" alt="" class="prodImg">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->name }}</h5>
                                <h5 class="align-middle">₱ {{ number_format($item->price,2) }}</h5>
                                <a href="{{ route('products.show', ['id' => $item->id]) }}" class="btn btn-block btn-primary"><i class="fas fa-shopping-cart"></i> Buy Now </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </main>

@endsection 