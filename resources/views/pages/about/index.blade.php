@extends('layouts.app')

@section('content')

    <main>
        <div class="container">
            <div class="row">
                <div class="col mt-5"> 
                    <div class="text-center mt-5">
                        <h3 class="mb-5">SHOP FASHION FOR MEN AND WOMEN ONLINE ON THE DEMO SHOP</h3>
                        <p>The DEMO SHOP is an online shopping for fashion that sells top local and international brands tailored for consumers in the Philippines. Our selection of over thousands of products covers every aspect of fashion, from shirts to dresses, sneakers to slip-ons, sportswear to watches, and so much more.</p>
                        <p>Start your style journey by owning a well-rounded range of basics and essentials. Think t-shirts and denim jeans, summer shorts and sunglasses, and other everyday necessities. Find new inspiration through our fashion-focused edits, ranging from spotlight-stealing evening wear and party-ready dresses, to florals and other on-trend prints, to athleisure and off-duty wear. Stay sharp in suits and blazers made for the office and beyond. Don’t forget to match your outfit with the right shoes, be they classic flats, high heels, boots, or leather shoes. Prep yourself for all seasons by shopping from a complete range of outerwear, from lightweight jackets to cozy sweaters to heavy-duty coats. Our fashion collections are complemented by all kinds of accessories; think bags, watches, and sports lifestyle gear, as well as the best in beauty and grooming, all found in one online shop.</p>
                        <p>With the widest selection of fashion you can possibly find, and outstanding services like lightning-quick delivery, free shipping, a host of payment methods like cash on delivery, and free returns, shopping online has never been easier!</p>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection