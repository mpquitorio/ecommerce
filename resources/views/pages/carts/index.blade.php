@extends('layouts.app')

@section('content')

    <main>
        <div class="container">
            <div class="row">     
                @if( !empty(session('cart')) )

                    @php
                        $total = 0;
                    @endphp
                    
                    <div class="col mt-5"> 
                        <table class="table mt-5">
                            <tr class="table-active">
                                <td class="p-3" width="50%">DemoShop ( {{ count($items) }} @php (count($items) > 1) ? print('items') : print('item') @endphp )</td>
                                <td class="text-right p-3" width="50%">Shipping Cost: FREE</td>
                            </tr>
                        </table>
                        <div class="table-responsive">
                            <table class="table table-stripped">
                                @foreach ($items as $item)

                                    @php
                                        $subtotal = $item->price * session('cart.'.$item->id);
                                        $total += $subtotal;
                                    @endphp

                                    <tr class="mb-5 border-bottom">
                                        <td class="p-1" width="10%">
                                            <img src="{{ $item->img_path }}" alt="" width="100%" height="75px" style="border: 1px solid #ddd">
                                        </td>
                                        <td class="p-2" width="50%">
                                            <strong class="font-weight-bold">{{ $item->name }}</strong> <br>
                                            <small>₱ {{ number_format($item->price, 2) }}</small>
                                        </td>
                                        <td class="p-2" width="15%">
                                            <input type="number" class="form-control quantity qty" min="1" max="10" data-product-id="{{ $item->id }}" data-price="{{ $item->price }}" value="{{ session("cart.".$item->id) }}">
                                        </td>
                                        <td class="text-right p-2" width="20%"><small>₱</small> <small class="sub">{{ number_format($subtotal, 2) }}</small></td>
                                        <td class="text-center p-2" width="5%">
                                            <a href="{{ route('carts.remove', ['id' => $item->id]) }}"><span class="text-danger font-weight-bold"><i class="fas fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="my-4 bg-light p-5">
                            <h5 class="font-weight-bold">Frequently Asked Questions</h5>
                            <div class="mt-4">
                                <h6 class="font-weight-bold"><i class="fas fa-shipping-fast"></i> Free Shipping Eligibility</h6>
                                <small class="text-muted">Enjoy FREE shipping.</small>
                            </div>
                            <div class="mt-4">
                                <h6 class="font-weight-bold"><i class="fas fa-undo-alt"></i> Returns Policy</h6>
                                <small class="text-muted">DEMOSHOP offers free and easy returns within 30 days of delivery. Simply ensure that items do not fall under this list of non-exchangeable/non-refundable brands and items. Items to be returned must meet the following conditions:</small>
                                <ul class="list-group ml-4 mt-2">
                                    <li><small class="text-muted">Within 30 days of receipt</small></li>
                                    <li><small class="text-muted">In original condition with tags intact</small></li>
                                    <li><small class="text-muted">In original packaging</small></li>
                                    <li><small class="text-muted">Accompanied by returns slip</small></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col col-lg-3 mt-5">
                        <table class="table mt-5">
                            <tr class="table-active">
                                <td class="p-3 text-center" colspan="2">
                                    <h6>APPLY PROMO CODE</h6>
                                    {{-- <form method="POST" action="">
                                        @csrf --}}
                                        <div class="d-flex d-inline">
                                            <input type="text" name="promoCode" class="form-control p-1" placeholder="Enter Promo Code" required>
                                            <button type="submit" class="btn btn-block btn-primary col-lg-4 p-1">APPLY</button>
                                        </div>
                                    {{-- </form> --}}
                                </td>
                            </tr>
                            <tr>
                                <td class="p-3"><h6 class="text-muted">Subtotal</h6></td>
                                <td class="p-3 text-right"><h6 class="text-muted" id="subtotal">₱ {{ number_format($total, 2) }}</h6></td>
                            </tr>
                            <tr>
                                <td class="p-3"><h6 class="text-muted">Shipping</h6></td>
                                <td class="p-3 text-right"><h6 class="text-muted">FREE</h6></td>
                            </tr>
                            <tr>
                                <td class="p-3"><h6>Grand<br>Total</h6></td>
                                <td class="p-3 text-right"><h6 id="total">₱ {{ number_format($total, 2) }}</h6></td>
                            </tr>
                            <tr>
                                <td class="p-0" colspan="2"><a href="{{ route('carts.empty') }}" class="btn btn-block btn-danger">Empty Your Cart</a></td>
                            </tr>
                            <tr>
                                <td class="p-0" colspan="2">
                                    <form action="carts/checkout/cash" method="POST">
                                        @csrf
                                        <input type="hidden" name="total" value="{{ $total }}">
                                        <button class="btn btn-block btn-primary">Proceed to Checkout</button>
                                    </form>
                                </td>
                            </tr>
                        </table>

                        <form action="carts/checkout/paypal" hidden id="form-checkout-paypal" method="POST">
                            @csrf
                            <input type="hidden" name="total" value='{{ $total }}'>
                        </form>
                        <div id="paypal-button-container" class="mt-2"></div>
                    </div>
                    <script>

                        const btnPaypalStyle = {
                            height: 38,
                            shape: 'rect',
                            layout: 'horizontal'
                        };
                        
                        paypal.Buttons({ 
                            style: btnPaypalStyle,
                            createOrder: function(data, actions) {
                                // Sets up transaction details including amount and item details.
                                return actions.order.create({
                                    purchase_units: [{
                                        amount: {
                                            value: '{{ $total }}'
                                        }
                                    }]
                                })
                            },
                            onApprove: function(data, actions) {
                                return actions.order.capture().then(function(details) {
                                    // This function shows a transaction success message to your buyer.
                                    alert('Transaction completed by ' + details.payer.name.given_name);
                                    document.querySelector('#form-checkout-paypal').submit();
                                })
                            }
                        }).render('#paypal-button-container');

                    </script>
                @else
                    <div class="col mt-5"> 
                        <div class="text-center mt-5">
                            <h1>Your Cart</h1>
                            <hr>
                            <h1 class="mt-5"><i class="fas fa-shopping-cart"></i></h1>
                            <h4>No products in the cart.</h4>
                            <small>Before proceed to checkout you must add some products to your shopping cart.<br> You will find a lot of interesting products on our "Catalog" page.</small>
                            <p class="mt-5"><a href="{{ route('products') }}" class="btn btn-outline-primary">Continue Shopping</a></p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </main>

@endsection