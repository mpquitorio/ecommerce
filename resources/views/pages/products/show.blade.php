@extends('layouts.app')

@section('content')

    <main>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 mt-5">
                    <img src="{{ $item->img_path }}" alt="" class="mt-5" style="width: 100%; height: 250px; border-bottom: 1px solid #ddd;">
                </div>
                <div class="col-lg-9 my-5">
                    <h2 class="mt-5">PRODUCT INFORMATION</h2>
                    <hr>
                    <h4>{{ $item->name }}</h4>
                    <p class="text-justify">{{ $item->description }}</p>
                    <h5>₱ {{ number_format($item->price,2) }}</h5>
                    <form action="{{ route('carts.add', ['id' => $item->id]) }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col d-flex d-inline">
                                <input type="number" class="form-control col-lg-1 p-3 quantity" min="1" max="10" value="1" name="qty" id="qty">
                                <button type="submit" class="btn btn-block btn-primary col-lg-3 ml-2"><i class="fas fa-shopping-cart"></i> Add to Cart </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

@endsection