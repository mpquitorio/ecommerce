@extends('layouts.app')

@section('content')
    
    <main>
        <div class="container">
            <div class="row">
                <!-- Catalogue -->
                <div class="col col-sm-12 col-md-6 col-lg-2 mt-5">
                    <h4 class="mt-5">Collection</h4>
                    <div class="list-group">
                        <a href="{{ route('products') }}" class="list-group-item list-group-item-action"><i class="fas fa-chevron-circle-right"></i> All </a>
                        @foreach ($categories as $category)
                        <a href="{{ route('selectedCategory', ['id' => $category->id]) }}" class="list-group-item list-group-item-action"><i class="fas fa-chevron-circle-right"></i> {{ $category->name }}</a>
                        @endforeach
                    </div>
                </div>
                <!-- Search Box -->
                <div class="col col-sm-12 col-md-6 col-lg-10 mt-5">
                    <div class="form-group mt-5">
                        <form method="GET" action="{{ route('products.search') }}" class="form-group">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="search" name="search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default input-group-text"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            @if( count($items) > 0 )
                                @foreach ($items as $item)
                                    <div class="col col-lg-4 mb-3 d-inline-block">
                                        <div class="card h-100">
                                            <img src="{{ $item->img_path }}" alt="" class="prodImg">
                                            <div class="card-body">
                                                <h5 class="card-title">{{ $item->name }}</h5>
                                                <h5 class="align-left">₱ {{ number_format($item->price, 2) }}</h5>
                                                <a href="{{ route('products.show', ['id' => $item->id]) }}" class="btn btn-block btn-primary"><i class="fas fa-shopping-cart"></i> Buy Now </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="col col-lg-12 mb-3 d-inline-block">
                                    <div class="text-center mt-5">
                                        <h4>Sorry! Product is not available yet.</h4>
                                        <small>Try to browse to other categories. </small>
                                    </div>
                                </div>
                            @endif
                        </div>
                        {{ $items->links() }}
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection