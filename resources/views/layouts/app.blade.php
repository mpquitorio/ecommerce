<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Michael P. Quitorio">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Title -->
        <title>{{ config('app.name') }}</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('images/icons/favicon.ico') }}">

        <!-- Fontawesome -->
        <script src="https://kit.fontawesome.com/54dd48c648.js" crossorigin="anonymous"></script>

        <!-- Bootswatch CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <!-- Toastr CSS -->
        <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">

        <!-- Animate CSS-->
        <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

        <!-- Wow JS -->
        <script src="{{ asset('js/wow.js') }}"></script>

        <!-- Boostrap JS -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    </head>
    <body>
        <script src="https://www.paypal.com/sdk/js?client-id=AVvmORIOIFoX2bc1dhCDMVxjl4_eefhT10pzpa9gqRiBkKC2t8nXFX0Yp7nv-2ZL4u-zejI4BIDiA2V9&currency=PHP"></script>
        @include('inc.navbar')
        @yield('content')
        @include('inc.footer')
  
        <!-- jQuery -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <!-- Popper -->
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <!-- Toastr JS -->
        <script src="{{ asset('js/toastr.min.js') }}"></script>
        <!-- Custom JS -->
        <script src="{{ asset('js/script.js') }}"></script>
        
        <script type="text/javascript">
            @if(Session::has('success'))
                toastr.success(" {{ Session::get('success') }} ")
            @endif
        </script>
    </body>
</html>
