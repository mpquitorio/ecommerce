@extends('layouts.app')

@section('content')

    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-3 mt-5">
                    <div class="card mt-5">
                        <div class="card-header font-weight-bold">Menu</div>
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item"><a href="" class="font-weight-bold"><i class="fas fa-list-alt"></i> My Purchases</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 my-5">
                    <div class="card mt-5">
                        <div class="card-header font-weight-bold"> Past Orders</div>

                        <div class="card-body">
                            @if ( count($orders) > 0 )
                                <!-- accordion -->
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        @foreach ($orders as $order)
                                            <div class="card-header" id="headingOne">
                                                <div class="mb-0 ">
                                                    <span class="float-right font-weight-bold d-flex d-inline-block">Orders Total: <h4 class="ml-2"> ₱ {{ number_format($order->price, 2) }}</h4></span>
                                                    <h6><img src="{{ asset('images/demoshop_logo.png') }}" alt="The DemoShop Logo" width="20px"> {{ config('app.name') }}</h6>
                                                    <button class="btn btn-link p-0 mt-2" type="button" data-toggle="collapse" data-target="#collapseOne{{$order->id}}" aria-expanded="true" aria-controls="collapseOne">
                                                        <h5 class="font-weight-bold">Order No: {{ $order->id }}</h5>
                                                    </button>
                                                </div>
                                                <small class="float-right text-success font-weight-bold"><i class="far fa-thumbs-up"></i> ORDERS COMPLETED</small>
                                                <small class="text-muted">Ordered last {{ $order->created_at->format('M d, Y @ h:i A')}}</small>
                                            </div>
                                            <div id="collapseOne{{$order->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body pb-0">
                                                    <table class="table">
                                                        @foreach ($datas as $data)
                                                            @if ($order->id == $data->id)
                                                                <tr>
                                                                    <td width="10%" class="p-2">{{ $data->qty }}x</td>
                                                                    <td width="70%" class="p-2">{{ $data->name }} <br> <small class="text-muted">@ ₱{{ number_format($data->price,2) }}</small></td>
                                                                    <td width="20%" class="p-2 text-right">₱ {{ number_format($data->qty * $data->price, 2) }}</span></td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                        <tr>
                                                            <td colspan="3" class="p-1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="80%" colspan="2" class="p-2">Subtotal <br> Delivery Fee</td>
                                                            <td width="20%" class="p-2 text-right">₱ {{ number_format($order->price, 2) }} <br> ₱ 0.00</td>
                                                        </tr>
                                                        <tr class="font-weight-bold" style="font-size:15px">
                                                            <td width="80%" colspan="2" class="p-2">Total <small>(incl.VAT)</small></td>
                                                            <td width="20%" class="p-2 text-right">₱ {{ number_format($order->price, 2) }}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!-- end accordion -->
                            @else
                                <div class="text-center mt-3">
                                    <h1><i class="fas fa-sad-cry"></i></h1>
                                    <h4>No orders available.</h4>
                                    <small>Add some products to your shopping cart.<br> You will find a lot of interesting products on our "Catalog" page.</small>
                                    <p class="mt-5"><a href="{{ route('products') }}" class="btn btn-outline-primary">Continue Shopping</a></p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection
